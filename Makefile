I = -i

all:
	@echo "Run: make install"

install:
	install -D -t $(DESTDIR)/usr/local/bin/ xpra
	install -D -t $(DESTDIR)/usr/local/libexec/ systemctl-start-xpra start-xpra-service
	install -D -t $(DESTDIR)/etc/systemd/system xpra@.service
#	install -D -t $(DESTDIR)/etc/pam.d pam.d/xpra
	install -D sudoers $(DESTDIR)/etc/sudoers.d/xpra
ifneq ($(DPKG_BUILD),1)
	systemctl daemon-reload
	which inotifywait > /dev/null || apt install inotify-tools
endif
