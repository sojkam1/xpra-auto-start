# Xpra autostart scripts

These scripts allow to start xpra server automatically when a user
logs in via SSH from xpra_launcher (GUI client).

For details see the comments in various files. They "execute" in this
order:
1. [xpra](xpra) script
2. [systemctl-start-xpra](systemctl-start-xpra)
3. [xpra@.service](xpra@.service)
4. [start-xpra-service](start-xpra-service)
5. Real XPRA server.

You can install it by running:

    make
